# Chameleon Admin - Modern Bootstrap 4 WebApp & Dashboard HTML Template + UI Kit

V1.1 : [03/12/2018]
[Updated]
- Updated theme to latest bootstrap v4.3.1
[Fixed]
- Fixed horizontal layout menu issue for small screen
- Small issues and bug fix


V1.0 : [13/07/2018] 
- Initial Release